package in.prabhoo.settings;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.telephony.TelephonyManager;

public class Scheduler extends JobService {

    public static final int JOB_ID = 10001;

    private TelephonyManager mTelephonyManager;
    private NetworkDelegate mNetworkDelegate;

    @Override
    public boolean onStartJob(JobParameters params) {
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        getNetworkDelegate().setPreferredNetwork(mTelephonyManager, 0);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private NetworkDelegate getNetworkDelegate() {
        if (mNetworkDelegate == null) {
            mNetworkDelegate = NetworkDelegate.create(this);
        }
        return mNetworkDelegate;
    }

}
