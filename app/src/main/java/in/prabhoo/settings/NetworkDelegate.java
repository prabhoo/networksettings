package in.prabhoo.settings;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class NetworkDelegate {

    public static NetworkDelegate create(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return new NetworkDelegateV24();
        } else {
            return new NetworkDelegateV24();
        }
    }

    public static class NetworkDelegateV24 extends NetworkDelegate {

        @Override
        public int getPreferredNetwork(TelephonyManager tm) {
            Method methodSubId = getHiddenMethod("getSubId", TelephonyManager.class, null);
            Method methodPreferredNetworkType = getHiddenMethod("getPreferredNetworkType", TelephonyManager.class, new Class[] {int.class});
            int preferredNetwork = -1000;
            try {
                int subId = (int) methodSubId.invoke(tm);
                preferredNetwork = (int) methodPreferredNetworkType.invoke(tm, subId);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return preferredNetwork;
        }

        @Override
        public void setPreferredNetwork(TelephonyManager tm, int network) {
            try {
                Method methodSubId = getHiddenMethod("getSubId", TelephonyManager.class, null);
                int subId = (int) methodSubId.invoke(tm);
                Method setPreferredNetwork = getHiddenMethod("setPreferredNetworkType",
                        TelephonyManager.class, new Class[] {int.class, int.class});
                Boolean success = (Boolean)setPreferredNetwork.invoke(tm,
                        subId, network);
                Log.i("Prabhoo", "Could set Network Type ::: " + (success.booleanValue() ? "YES" : "NO"));
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static class NetworkDelegateV23 extends NetworkDelegate {


        @Override
        public int getPreferredNetwork(TelephonyManager tm) {
            Method method = getHiddenMethod("getPreferredNetworkType", TelephonyManager.class, null);
            int preferredNetwork = -1000;
            try {
                preferredNetwork = (int) method.invoke(tm);
                Log.i("Prabhoo", "Preferred Network is ::: " + preferredNetwork);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            return preferredNetwork;
        }

        @Override
        public void setPreferredNetwork(TelephonyManager tm, int network) {
            try {
                Method setPreferredNetwork = getHiddenMethod("setPreferredNetworkType",
                        TelephonyManager.class, new Class[] {int.class});
                Boolean success = (Boolean) setPreferredNetwork.invoke(tm, network);
                Log.i("Prabhoo", "Could set Network Type ::: " + (success.booleanValue() ? "YES" : "NO"));
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }

    protected Method getHiddenMethod(String methodName, Class fromClass, Class[] params) {
        Method method = null;
        try {
            Class clazz = Class.forName(fromClass.getName());
            method = clazz.getDeclaredMethod(methodName, params);
            method.setAccessible(true);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return method;
    }


    public abstract int getPreferredNetwork(TelephonyManager tm);

    public abstract void setPreferredNetwork(TelephonyManager tm, int network);

}
