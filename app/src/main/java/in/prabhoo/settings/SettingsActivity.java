package in.prabhoo.settings;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;

import android.telephony.TelephonyManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.List;


public class SettingsActivity extends AppCompatPreferenceActivity {



    /*public int getPreferredNetwork() {
        Method methodSubId = getHiddenMethod("getSubId", TelephonyManager.class, null);
        Method methodPreferredNetworkType = getHiddenMethod("getPreferredNetworkType", TelephonyManager.class, new Class[] {int.class});
        int preferredNetwork = -1000;
        try {
            int subId = (int) methodSubId.invoke(mTelephonyManager);
            preferredNetwork = (int) methodPreferredNetworkType.invoke(mTelephonyManager, subId);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return preferredNetwork;
    }

    public void setPreferredNetwork(int networkType) {
        try {
            Method setPreferredNetwork = getHiddenMethod("setPreferredNetworkType",
                    TelephonyManager.class, new Class[] {int.class});
            Boolean success = (Boolean)setPreferredNetwork.invoke(mTelephonyManager,
                    networkType);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows data and sync preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DataSyncPreferenceFragment extends PreferenceFragment {


        private TelephonyManager mTelephonyManager;
        private JobScheduler mJobScheduler;
        private NetworkDelegate mNetworkDelegate;
        private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                String stringValue = value.toString();
                if (preference instanceof ListPreference) {
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);
                    preference.setSummary(
                            index >= 0
                                    ? listPreference.getEntries()[index]
                                    : null);
                    if (listPreference.getKey().equalsIgnoreCase("sync_frequency")) {
                        scheduleWithPermission(Long.valueOf(stringValue) * 60 * 1000);
                        Log.d("Settings", "sync_frequency : " + stringValue);
                    } else if (listPreference.getKey().equalsIgnoreCase("network_selected")) {
                        getNetworkDelegate().setPreferredNetwork(mTelephonyManager, 0);
                        Log.d("Settings", "sync_frequency : " + stringValue);
                    }
                } else {
                    preference.setSummary(stringValue);
                }
                return true;
            }
        };


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);
            mTelephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
            bindPreferenceSummaryToValue(findPreference("network_selected"));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.
         *
         * @see #sBindPreferenceSummaryToValueListener
         */
        private void bindPreferenceSummaryToValue(Preference preference) {
            // Set the listener to watch for value changes.
            preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        }

        private void scheduleWithPermission(long interval) {
            schedule(interval);
            if (!checkIfAlreadyhavePermission(Manifest.permission.RECEIVE_BOOT_COMPLETED)) {
                requestForSpecificPermission(new String [] {Manifest.permission.RECEIVE_BOOT_COMPLETED});
            }
        }

        private void schedule(long interval) {
            mJobScheduler = (JobScheduler) getActivity().getSystemService( Context.JOB_SCHEDULER_SERVICE );
            JobInfo.Builder builder = new JobInfo.Builder(Scheduler.JOB_ID,
                    new ComponentName( getActivity().getPackageName(),
                            Scheduler.class.getName()));
            builder.setPeriodic(interval);
            if (checkIfAlreadyhavePermission(Manifest.permission.RECEIVE_BOOT_COMPLETED)) {
                builder.setPersisted(true);
            }
            if(mJobScheduler.schedule(builder.build()) <= JobScheduler.RESULT_SUCCESS) {
                Log.d("Settings", "Periodic Job Scheduled.");
            }
        }

        private boolean checkIfAlreadyhavePermission(String permission) {
            int result = ContextCompat.checkSelfPermission(getActivity(), permission);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }

        private void requestForSpecificPermission(String[] permissions) {
            ActivityCompat.requestPermissions(getActivity(), permissions, 101);
        }

        private NetworkDelegate getNetworkDelegate() {
            if (mNetworkDelegate == null) {
                mNetworkDelegate = NetworkDelegate.create(getActivity());
            }
            return mNetworkDelegate;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   Log.d("Settings", "Permission Granted");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
